/* 
   1. У Java Script існують такі типи даних: Boolean, String, Number,  Undefined, Null та Object.
   2. == - це не строга рівність, === - це строга рівність.
   3. Оператори - це функції JavaScript. Ми використовуємо оператор щоб запустити ту чи іншу функцію, 
    кожна така дія чи функція має свого оператора.
*/

let name = prompt("What is your name?");
while (name === null || name.trim() === "") {
  name = prompt("What is your name?", /*name === null ? "" : name */);
}


let age = Number(prompt("How old are you?"));
while (Number.isNaN(age) || age === null || name === "" ) {
  age = Number(prompt("How old are you?"));
}

if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age >= 18 && age <= 22) {
  // let a = confirm("Are you sure you want to continue?");
  // alert(`Hello, ${name}`);
  if (confirm("Are you sure you want to continue?")) {
    alert(`Hello, ${name}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert(`Hello, ${name}`);
}
