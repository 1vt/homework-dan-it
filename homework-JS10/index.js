function allTabs() {
  document.querySelector(".tabs-title").classList.add("active");
  document.querySelector(".tab-content").classList.add("active");
  
  function selectTab(e) {
    const target = e.target.dataset.target

    document.querySelectorAll(".tabs-title, .tab-content").forEach((el) =>
     {el.classList.remove("active");});
    
     e.target.classList.add('active')
    // console.log(document.querySelector('.' + target))
    document.querySelector('.' + target).classList.add('active') 
  }

  document.querySelectorAll(".tabs-title").forEach((el) => {
    el.addEventListener("click", selectTab);
  });
}
allTabs()