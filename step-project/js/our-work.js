function allTabs() {
  document.querySelector(".tabs-title").classList.add("active");
  document.querySelector(".tab-content").classList.add("active");
  
  function selectTab(e) {
    const target = e.target.dataset.target

    document.querySelectorAll(".tabs-title, .tab-content").forEach((el) =>
     {el.classList.remove("active");});
    
     e.target.classList.add('active')
    document.querySelector('.' + target).classList.add('active') 
  }

  document.querySelectorAll(".tabs-title").forEach((el) => {
    el.addEventListener("click", selectTab);
  });
}
allTabs()



const tabs = document.querySelectorAll('.Wtabs-title-pht');
const tabContents = document.querySelectorAll('.Wtab-content-pht');
tabs.forEach(tab => {
  tab.addEventListener('click', () => {
    tabContents.forEach(content => {
      content.style.display = 'none';
    }); 
    const target = tab.dataset.target;
    const targetContent = document.querySelector(`#${target}`);
    targetContent.style.display = 'block';
  });
});




const loadMoreButton = document.querySelector(".our-amazing-work-button");
const photosContainer = document.querySelector(".Wtab-content-pht.active");
const newPhotos = [
  "../img/Layer 28.png",
  "../img/Layer 28.png",
  "../img/Layer 28.png",
  "../img/Layer 28.png",
  "../img/Layer 28.png",
  "../img/Layer 28.png",
  "../img/Layer 28.png",
  "../img/Layer 28.png",
  "../img/Layer 28.png",
  "../img/Layer 28.png",
  "../img/Layer 28.png",
  "../img/Layer 28.png",
];
loadMoreButton.addEventListener("click", () => {
  newPhotos.forEach((src) => {
    const newPhoto = document.createElement("span");
    newPhoto.classList.add("our-amazing-work-item");
    newPhoto.style.backgroundImage = `url(${src})`;
    photosContainer.appendChild(newPhoto);
  });

  if (document.querySelectorAll(".Wtab-content-pht.active > span").length === 18) {
   ;
  }loadMoreButton.style.display = "none"
});






const testimonials = [
  {
    name: "Hasan Ali",
    job: "UX Designer",
    image: "./img/Layer 5.png",
    text: " Integer dignissim, augue tempus ultricies luctus, quam dui laoreetsem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem,non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
  },
  {
    name: "Марина",
    job: "Web Developer",
    image: "./img/Layer 8.png",
    text: "Пише ідеально ідеально ідеально ідеально ідеально ідеально ідеально ідеально ідеально ідеально ЧИСТИЙ КОД.",
  },
  {
    name: "Антоха",
    job: "Marketing Manager",
    image: "./img/Layer 7.png",
    text: "Максимально максимально максимально максимально максимально максимально максимально максимально максимально КРУТИЙ МЕНЕДЖЕР.",
  },
  {
    name: "Марія",
    job: "Product Manager",
    image: "./img/Layer 6.png",
    text: " Дуже дуже дуже Дуже дуже дуже Дуже дуже дуже Дуже дуже дуже Дуже дуже дужеДуже дуже дуже РОЗУМНА ЖІНКА.",
  },
];

const carousel = document.querySelector(".people-say-line");
const personName = document.querySelector(".people-say-people-name");
const personJob = document.querySelector(".people-say-people-job");
const personImage = document.querySelector(".person");
const testimonialText = document.querySelector(".people-say-text");
let currentTestimonial = 0;
function showTestimonial() {
  const testimonial = testimonials[currentTestimonial];

  personName.textContent = testimonial.name;
  personJob.textContent = testimonial.job;
  personImage.src = testimonial.image;
  testimonialText.textContent = testimonial.text;

  currentTestimonial++;

  if (currentTestimonial >= testimonials.length) {
    currentTestimonial = 0;
  }
}
setInterval(showTestimonial, 2000);
