/*
1. Екранування - це коли вставляють спеціальні символи наприклад зворотній слеш
і завдяки цим символам ми даємо зрозуміти що цей текст
повинен трактуватись як звичайні символи.

2. Спосіб оголошення функції:
function name(object) {
  тіло функції;
}
Також є стрілкові функції

3. Hoisting — механізм який перед початком роботи самого коду
пересуває вгору своєї області видимості змінні та оголошення функцій. 
*/

function createNewUser() {
  const name = prompt("Your name?");
  const surname = prompt("Your surname?");
  const birthday = prompt("Your birthday in dd.mm.yyyy?")
  const arrayDate = birthday.split('.')
 // const formatString =` ${arrayDate[2]}-${arrayDate[1]}-${arrayDate[0]} ` 
  const user = {
    name,
    surname,
    birthday, 
    /*getLogin() {
      return name.toLowerCase()[0] + surname.toLowerCase();
    }, */
    /*getDateFromUser() {
    birthday = prompt("Your birthday in dd.mm.yyyy?");
    }, */
    getAge() {
      const today = new Date();
     // const age = today.getFullYear() - this.birthday.getFullYear();
     // const month = today.getMonth() - this.birthday.getMonth();
      const age = "24"
      const month = "10"
        if (month < 0 || (month === 0 && today.getDate() < this.birthday.getDate())) {
        return age - 1;
      }
      return age;
    },
    getPassword() {
      const birthYear = this.birthday.split('.')[2];
      //const birthYear = birthday.getFullYear();
      return `${this.name[0].toUpperCase()}${this.surname.toLowerCase()}${birthYear}`;
    }
  };
  return user;
}

const user = createNewUser();
console.log(user);
console.log(`${user.getAge()}`);
console.log(`${user.getPassword()}`);
