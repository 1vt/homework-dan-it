function pressButton(event) {
  const key = event.key.toLowerCase();
  const buttons = document.querySelectorAll('.btn');
  buttons.forEach((button) => {
    if (button.dataset.key === key) {
      button.style.backgroundColor = 'blue';
    } 
    
  });
}

function unPressButton(event) {
  const key = event.key.toLowerCase();
  const buttons = document.querySelectorAll('.btn');
  buttons.forEach((button) => {
    if (button.dataset.key === key) {
      button.style.backgroundColor = '#000000';
    }
  });
}

window.addEventListener('keydown', pressButton);
window.addEventListener('keyup', unPressButton);
