function showPassword() {
  const openEye = document.querySelector(".fa-eye");
  const closeEye = document.querySelector(".fa-eye-slash");
  const input = document.querySelector(".password-input");
  console.log(closeEye);
  closeEye.addEventListener("click", () => {
    closeEye.classList.toggle("active");

    if (input.getAttribute("type") === "password") {
      input.setAttribute("type", "text");
    } else {
      input.setAttribute("type", "password");
    }
  });

  const openEyeConf = document.querySelector(".fa-eye-conf");
  const closeEyeConf = document.querySelector(".fa-eye-slash-conf");
  const inputConf = document.querySelector(".password-input-conf");
  console.log(closeEyeConf);
  closeEyeConf.addEventListener("click", () => {
    closeEyeConf.classList.toggle("active");

    if (inputConf.getAttribute("type") === "password") {
      inputConf.setAttribute("type", "text");
    } else {
      inputConf.setAttribute("type", "password");
    }
  });

  const btn = document.querySelector(".btn");
  btn.addEventListener("click", () => {
    const field1Value = document.getElementById("field1").value;
    const field2Value = document.getElementById("field2").value;
    if (field1Value === field2Value) {
      alert("You are welcome");
    } else {
      const newDiv = document.createElement("div");
      newDiv.style.color = "red";
      const text = document.createTextNode("Потрібно ввести однакові значення");
      newDiv.appendChild(text);
      const currentDiv = document.getElementById("div1");
      document.body.insertBefore(newDiv, currentDiv);
      
      /*
      const errorMessage = document.createElement('div');
      errorMessage.innerText = "Потрібно ввести однакові значення";
      errorMessage.style.color = "red"; 
      */
    }
  });
}
showPassword();
