const images = document.querySelectorAll(".image-to-show");
      const stopButton = document.querySelector("#stop-button");
      const resumeButton = document.querySelector("#resume-button");

      let currentIndex = 0;
      let intervalId;

      function showCurrentImage() {
        for (let i = 0; i < images.length; i++) {
          if (i === currentIndex) {
            images[i].style.display = "block";
          } else {
            images[i].style.display = "none";
          }
        }

        currentIndex++;

        if (currentIndex === images.length) {
          currentIndex = 0;
        }
      }
     
      function startSlideshow() {
        intervalId = setInterval(showCurrentImage, 3000);
      }

      startSlideshow();

      function stopSlideshow() {
        clearInterval(intervalId);
      }

      function resumeSlideshow() {
        startSlideshow();
      }
      resumeButton.addEventListener("click", resumeSlideshow);
      stopButton.addEventListener("click", stopSlideshow);