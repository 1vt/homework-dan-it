/*
1. DOM - об'єктна модель документа. DOM створює браузер, 
завдяки ньому ми легко можемо знайти потрібний елемент і взаємодіяти з ним.

2. innerText повертає весь текст, що міститься в елементі, і всі його дочірні елементи,
а от innerHtml повертає весь текст, включаючи теги html, який міститься в елементі.

3. Всього є 6 способів: getElementById, getElementByClassName, getElementByTagName, getElementByName
і наступні два вважаюьбся кращими: querySelectorAll та querySelector.
*/
//1

  //const p = document.getElementsByTagName("p")
  //p.style.backgroundColor = "#ff0000";
//p.style = "background-color: #ff0000;";
//console.log(document.getElementsByTagName("p"))

const paragraphs = document.getElementsByTagName('p');
for (let i = 0; i < paragraphs.length; i++) {
  paragraphs[i].style.backgroundColor = "#ff0000";
}

//2
const optionsList = document.getElementById("optionsList")
console.log(optionsList)
const parentElement = optionsList.parentElement;
console.log(optionsList.parentElement)
console.log(optionsList.childNodes)

//3
const testParagraph = document.getElementById("testParagraph")
testParagraph.textContent ='This is a paragraph';

//4 

const mainHeader = document.querySelector('.main-header');
const childElements = mainHeader.children;
for (let i = 0; i < childElements.length; i++) {
  console.log(childElements[i]);
  childElements[i].classList.add('nav-item');
}


/*
const nodeList = document.body.childNodes;
console.log(nodeList);
const elements = document.getElementsByClassName("main-header");
elements.forEach(element => {
  element.classList.add("nav-item");
});
*/

//5
const section = document.querySelectorAll(".section-title")
for (let i = 0; i < sectionTitles.length; i++) {
  sectionTitles[i].classList.remove('section-title');
}
console.log(section)
//,0section.classList.remove("section-title")