/*
1. Метод об'єкта це функція яка належить об'єкту.
2. Властивість об'єкта можуть мати значення будь якого типу якому належить об'єкт.
3. Посилальний тип – це внутрішній тип мови. Він використовується з метою передачі інформації від крапки до дужок виклику.
*/

function createNewUser() {
  const name = prompt("Your name?");
  const surname = prompt("Your surname?");

  const user = {
    name,
    surname,
    getLogin() {
      return name.toLowerCase()[0] + surname.toLowerCase();
    },
  };
  return user;
}
console.log(createNewUser().getLogin());
