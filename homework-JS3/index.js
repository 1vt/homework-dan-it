/*
1. Цикли потрібні для виконання однакових дій багато разів.
Наприклад перебираючи цифри від 1 до 5, виконати для кожного однаковий код.
2. Цикл while для багаторазового повторення одного й того самого коду.
3. 

*/

/*
let ask = Number(prompt("Your number"));
while (Number.isNaN(ask)) {
  ask = Number(prompt("Your number")); }

let num = 5
console.log( ask / num )
*/

let number = prompt("Your number");

if (number < 5) {
  console.log("Sorry, no numbers")
} else {
  for(let n = 1; n <= number; n++ ) {
    if( n % 5 === 0) {
     
      console.log(n)
    }
  }
}

/*for(let n = 1; n <= number; n++ ) {
  if( n % 5 === 0) {
    isNumber = true
    console.log(n)
  }
}

if (!isNumber) {
  console.log("Sorry, no numbers")
}
*/