/* let switchMode = document.getElementById('switchClick')

switchMode.onclick = function () {
    let theme = document.getElementById('theme');

    if (theme.getAttribute('href') == './css/style.css') {
        theme.href = './css/dark-style.css'
    } else {
        theme.href = './css/style.css'
    }
}
*/

let switchMode = document.getElementById('switchClick');
let theme = document.getElementById('theme');
let savedTheme = localStorage.getItem('theme');

if (savedTheme) {
  theme.href = savedTheme;
}

switchMode.onclick = function () {
  if (theme.getAttribute('href') == './css/style.css') {
    theme.href = './css/dark-style.css';
    localStorage.setItem('theme', './css/dark-style.css');
  } else {
    theme.href = './css/style.css';
    localStorage.setItem('theme', './css/style.css');
  }
};