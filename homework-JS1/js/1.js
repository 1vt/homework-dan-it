/*
  1. Змінну можна оголосити двома способами: let та const, у випадку з const її змінити не можливо. Також є застарілий варіант через var.
  2. prompt користувач сам пише відповідь, а confirm тільки може обрати між варіантами: cancel or ok.
  3. Коли значення можуть змінюватись між різними типами автоматично. Приклад:  +'1999'
*/


let name = 'Vlad', admin = name;
console.log(name, admin)

let days = 3
let seconds = 86.400 * days;
console.log(seconds)

let text = prompt('Hey?');
console.log(text);