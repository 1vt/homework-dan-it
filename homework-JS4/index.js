/*
1. Функції потрібні для того щоб полегшити роботу з кодом,
 зробити його компактнішим та дозволити повторювати однакові дії декілька разів.
2. Ми передаємо агрумент в функцію щоб оголосити саму функціЮ.
3. Return - ця функція повертає значення, а потрібен цей оператор щоб функція зупинялась та повертала значення самоЇ функції.
*/

let num1 = +prompt("Your first number");
let num2 = +prompt("Your second number");
let oper = prompt("Enter operation");

function calcOperation() {
  switch (oper) {
    case "+":
      console.log(num1 + num2);
      break;
    case "-":
      console.log(num1 - num2);
      break;
    case "/":
      console.log(num1 / num2);
      break;
    case "*":
      console.log(num1 * num2);
      break;
  }
}

calcOperation()

/*    switch (oper) {
        case "+":
          console.log(num1 + num2);
          break;
        case "-":
          console.log(num1 - num2);
          break;
        case "/":
          console.log(num1 / num2);
          break;
        case "*":
          console.log(num1 * num2);
          break;
      }
*/
