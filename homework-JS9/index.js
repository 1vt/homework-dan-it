/*
1. 1)document.createElement(tag)
   2)document.createTextNode(text)

2. Метод insertAdjacentHTML дозволяє вставити рядок
HTML-коду в будь-яке місце сторінки.
Є такі способи вставки: beforeBegin, afterEnd, afterBegin, beforeEnd. 

3. Щоб видалити елементи потрібно викристати метод remove()
*/

function createList(arr, parent = document.body){
 const list = document.createElement("ul");
 arr.forEach((item) => { 
 const listItem = document.createElement('li');
 listItem.textContent = item;
 list.appendChild(listItem);
 });
 parent.appendChild(list);
}
const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
createList(arr);
